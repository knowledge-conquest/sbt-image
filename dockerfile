FROM openjdk:8

# Copy sbt distribution and launch script
WORKDIR /root
COPY sbt-dist/ .sbt-dist
COPY sbt .

# Create temporary folder to install sbt
WORKDIR /root/tmp

# Insall sbt
RUN ~/sbt sbtVersion

# Remove temporary folder
WORKDIR /root
RUN rm -rf tmp


CMD ["/bin/bash"]
